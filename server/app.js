const express = require('express');
const cors = require('cors');
const logger = require('morgan');

const app = express();

const corsOptions = {
    origin: 'http://localhost:8081'
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger('dev'));

const db = require('./models');
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log('Connected to the database!');
    })
    .catch(err => {
        console.log('Cannot connect to the database!', err);
        process.exit();
    });

app.get('/', (req, res) => {
    res.json({ message: 'Welcome to memo application.'});
});

const posts = require('./routes/postRoute');
const users = require('./routes/userRoute');

app.use('/api/v1', posts);
app.use('/api/v1', users);

const port = 8000;
app.listen(port, (err) => {
    if (err) {
        console.log(err);
    }
    console.log('Server is running on port:', port);
});