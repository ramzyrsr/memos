module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            email: String,
            password: String,
            username: String
        },
        { timestamps: true }
    );

    schema.method('toJSON', function() {
        const { _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const User = mongoose.model('user', schema);
    return User;
};