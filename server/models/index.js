const dbConfig = require('../config/db.config');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.post = require('./post')(mongoose);
db.user = require('./userModel')(mongoose);

module.exports = db;