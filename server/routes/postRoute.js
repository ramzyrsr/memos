const express = require('express');
const router = express.Router();
const controller = require('../controller/postController');

router
    .route('/memos')
    .get(controller.getAllMemos)
    .post(controller.createMemo);

router
    .route('/memo/:id')
    .get(controller.getMemo)
    .put(controller.updateMemo)
    .delete(controller.deleteMemo);

module.exports = router;