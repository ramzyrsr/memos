const express = require('express');
const router = express.Router();
const controller = require('../controller/userController');
const auth = require('../controller/authController');

router.post('/signup', auth.signup);
router.post('/login', auth.login);

router
    .route('/users')
    .get(controller.getAllUsers)
    .post(controller.createUser);

router
    .route('/user/:username')
    .get(controller.getUser)
    .delete(controller.deleteUser)
    
module.exports = router;