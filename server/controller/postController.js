const db = require('../models');
const Post = db.post;
const methods = {};

methods.getMemo = async (req, res) => {
    try {
        const id = req.params.id;
        const memo = await Post.findById(id);

        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: `Memo with id ${id} is found`,
            data: memo
        };

        if (!memo) {
            res
                .json({
                    statusCode: 404,
                    statusText: 'Not found',
                    message: `Not found Memo with id ${id}`
                })
                .status(404);

        } else {
            res
                .json(resPayload)
                .status(200);
        };

    } catch (error) {
        res
            .json({
                statusCode: 500,
                statusText: 'Error',
                message: `Error retrieving Memo with id: ${id}`
            })
            .status(500);
    }
};

methods.getAllMemos = async (req, res) => {
    try {
        const posts = await Post.find({});
        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'Get all memos',
            data: posts
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Error',
                message: 'Some error occured while retrieving memos.'
            })
            .status(400);
    }
}

methods.createMemo = async (req, res) => {
    try {
        if (!req.body.title) {
            res.status(400).send({ message: 'Content can not be empty!'});
            return;
        }

        const post = new Post({
            title: req.body.title,
            description: req.body.description
        });

        post
            .save(post);
        res
            .json({
                statusCode: 201,
                statusText: 'Created',
                message: 'Memo is created',
                data: post
            })
            .status(201);

    } catch (error) {
        res.json({
			statusCode: 400,
			statusText: 'Bad Request',
			message: error
		})
		.status(400);
    }
};

methods.updateMemo = async (req, res) => {
    try {
        if (!req.body) {
            res
                .json({
                    statusCode: 404,
                    statusText: 'Not Found',
                    message: 'Data to update can not be empty'
                })
                .status(404);
        };

        const id = req.params.id;
        const update = await Post.findByIdAndUpdate(id, req.body, { useFindAndModify: false });

        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'Memo successfully updated',
            data: update
        }

        if (!update) {
            res
                .json({
                    statusCode: 404,
                    statusText: 'Not Found',
                    message: `Cannot update Memo with id: ${id}. Maybe Memo was not found!`
                })
                .status(404);
        } else {
            res
                .json(resPayload)
                .status(200);
        }

    } catch (error) {
        res.json({
			statusCode: 400,
			statusText: 'Bad Request',
			message: error
		})
		.status(400);
    }
};

methods.deleteMemo = async (req, res) => {
    try {
        const id = req.params.id;
        const remove = await Post.findByIdAndRemove(id, { useFindAndModify: false });

        if (!remove) {
            res
                .json({
                    statusCode: 404,
                    statusText: 'Not Found',
                    message: `Cannot delete Memo with id: ${id}`
                })
                .status(404);
        } else {
            res
                .json({
                    statusCode: 204,
                    statusText: 'No Content',
                    message: 'Memo is deleted succesfully'
                })
                .status(204);
        }

    } catch (error) {
        res
            .json({
                statusCode: 500,
                statusText: 'Internal Server Error',
                message: error
            })
            .status(500);
    }
};

module.exports = methods;