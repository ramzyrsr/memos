const jwt = require('jsonwebtoken');
const passHash = require('password-hash');
const User = require('../models/userModel');

const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
};

exports.signup = async (req, res, next) => {
    try {
        const user = await User.find({ where: {
            email: req.body.email
        }});

        if (user) {
            res
                .json({
                    statusCode: 204,
                    statustext: 'success',
                    message: 'Email has already registered'
                })
                .status(204);
        } else {
            const hashedPass = passHash.generate(req.body.password);

            const newUser = await User.create({
                email: req.body.email,
                password: hashedPass,
                username: req.body.username
            });

            const token = signToken(newUser.id);

            res
                .json({
                    statusCode: 200,
                    statustext: 'success',
                    token: token,
                    data: newUser
                })
                .status(200);
        }
    } catch (error) {
        res
            .json({
                statusCode: 400,
                statustext: 'Error',
                message: error.message
            })
            .status(400);
    }
};

exports.login = async (req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email: req.body.email });
        const hashedPass = user.dataValues.password;
        const verify = await passHash.verify(password, passHash);

        const resPayload = {
            id: user.dataValues.id,
            username: user.dataValues.username
        };

        const token = signToken(newUser.id);

        if ( email && password) {
            if (!user || !verify) {
                res
                    .json({
                        statusCode: 401,
                        statustext: 'Incorrect email or password',
                        message: error.message
                    })
                    .status(401);
            } else {
                res
                    .json({
                        statusCode: 200,
                        statustext: 'success',
                        data: resPayload
                    })
                    .status(200);
            }
        }

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statustext: 'Error',
                message: error.message
            })
            .status(400);
    }
};