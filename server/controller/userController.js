const db = require('../models');
const User = db.user;
const passHash = require('password-hash');
const methods = {};

methods.getUser = async (req, res) => {
    try {
        // const nameBody = req.body.Name;
        // const nameQuery = req.query.Name;
        // const user = await User.find({
        //     Name: { $regex: new RegExp(nameBody || nameQuery), $options: 'i'}
        // });
        const x = req.params.username;
        const user = await User.find({ username: x})

        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: `${x} is found`,
            data: user
        };

        if (!user) {
            res
                .json({
                    statusCode: 404,
                    statusText: 'Not found',
                    message: `${user} is not found`
                })
                .status(404);
        } else {
            res
                .json(resPayload)
                .status(200);
        };

    } catch (error) {
        res
            .json({
                statusCode: 500,
                statusText: 'Error',
                message: `Error retrieving User with username ${user}`
            })
            .status(500);
    }
};

methods.getAllUsers = async (req, res) => {
    try {
        const users = await User.find({});
        const resPayload = {
            statusCode: 200,
            statusText: 'success',
            message: 'Get all users',
            data: users
        };
        res
            .json(resPayload)
            .status(200);

    } catch (error) {
        res
            .json({
                statusCode: 500,
                statusText: 'error',
                message: 'Some error occured while retrieving users.'
            })
            .status(500);
    }
};

methods.createUser = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email
        });

        if (!req.body) {
            res
                .json({
                    statusCode: 400,
                    statusText: 'Bad Request',
                    message: 'Content can not be empty!'
                })
                .status(400);
        };

        if (user) {
            res
                .json({
                    statusCode: 409,
                    statusText: 'Conflict',
                    message: 'Email has already registered'
                })
                .status(409);

        } else {
            const hashedPass = passHash.generate(req.body.password);
            
            const newUser = new User({
                email: req.body.email,
                password: hashedPass,
                username: req.body.username
            });
    
            newUser
                .save(newUser);
            res
                .json({
                    statusCode: 201,
                    statusText: 'Created',
                    message: 'User is created',
                    data: newUser
                })
                .status(201);
        }
        
    } catch (error) {
        res
            .json({
                statusCode: 500,
                statusText: 'error',
                message: 'Some error occured while retrieving users.'
            })
            .status(500);
    }
};

methods.deleteUser = async (req, res) => {
    try {
        const username = req.params.username
        await User.deleteOne({ username })
        
        res
            .json({
                statusCode: 204,
                statusText: 'No Content',
                message: `${username} is deleted`
            })
            .status(204);

    } catch (error) {
        res
            .json({
                statusCode: 400,
                statusText: 'Error',
                message: error
            })
            .status(400)
    }
};

module.exports = methods;