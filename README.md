# Memos

Memos is a Memo API with mongoDB database.

## Instalation
---
```
$ cd server
$ npm install
$ npm start
```

## How to use the API
---
API request must include both `api` and the API version. The API version is defined in `server/app.js`. For example, the root of the v1 API is at `/api/v1`.

The API uses JSON to serialize data. You don't need to specify `.json` at the end of the API URL.

## API List
---
|   Method  |   Endpoint    |   Description             |
|-----------|---------------|---------------------------|
| GET       | /memos        | List of memos             |
| POST      | /memos        | Create new memo           |
| GET       | /memo/:id     | Get memo by id            |
| PUT       | /memo/:id     | Update the existing memo  |
| DELETE    | /memo/:id     | Delete the existing memo  |

|   Method  |   Endpoint          |   Description                           |
|-----------|---------------------|-----------------------------------------|
| POST      | /signup             | Create a new user and generate token    |
| POST      | /login              | Sign in to existing user                |
| GET       | /users              | List of users                           |
| POST      | /users              | Create new user                         |
| GET       | /user/:username     | Get user by username                    |
| PUT       | /user/:username     | Update the existing user                |
| DELETE    | /user/:username     | Delete the existing user                |

## Contributing
---
Pull requests are welcome. For major changes, please open an issue first to discuss what you like to change.

Please make sure to update test as appropriate.